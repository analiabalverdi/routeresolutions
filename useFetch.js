import { useState, useEffect } from "react"

const useFetch = (url) => {
    const [data, setData] = useState(null) 

    useEffect( async () => {
        await fetch(url)
            .then(res => res.json())
            .then(result => setData(result.data))
            .catch(error => console.log(error))
    }, [url])

    return [data]
}

export default useFetch
