import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css'

import Layout from './pages/Layout'
import NoPage from './pages/Layout'
import Users from './components/Users';
import Messages from './components/Messages'



function App() {
  
  return (
      <div className='wrapper-app'>
        
        <BrowserRouter>
          <Routes>
            <Route path='/' element={<Layout/>}>
              <Route path='mensajes' element={<Messages/>}/>
              <Route path='usuarios' element={<Users/>}/>
              <Route path='*' element={<NoPage/>}/>
            </Route>
          </Routes>
        </BrowserRouter>
         

      </div>
  )
}
 
export default App;