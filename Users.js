import useFetch from "./useFetch";

const Users = () => {
  const [data] = useFetch("http://localhost:5000/users/")
  console.log(data);
  return (
    <>
      {data &&
        data.map((item) => {
          return <p key={item.username}>{item.username}</p>;
        })}
    </>
  );
};

export default Users;

