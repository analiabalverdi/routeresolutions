import useFetch from "./useFetch";

const Messages = () => {
  const [data] = useFetch("http://localhost:5000/messages/");
  console.log(data);
  return (
    <>
      {data &&
        data.map((item) => {
          return <p key={item.text}>{item.text}</p>;
        })}
    </>
  );
};

export default Messages;
