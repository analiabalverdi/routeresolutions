import { Outlet, Link } from "react-router-dom";
import Button from "@material-ui/core/Button";

const Layout = () => {
  return (
    <>
      <h1>Bienvenidos!</h1>

      <nav>
        <ul>
          <li>
            <Button variant="contained" color="success" >
              <Link to="usuarios">Usuarios</Link>
            </Button>
          </li>
          <br></br>
          <li>
            <Button variant="contained" color="success">
              <Link to="mensajes">Mensajes</Link>
            </Button>
          </li>
        </ul>
      </nav>
      <Outlet />
    </>
  );
};

export default Layout;
